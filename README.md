# otfforge

Convert OpenType font to TrueType format with FontForge.

## Requirements

* [FontForge](http://fontforge.github.io/en-US/)

## Installation

Install the module as CLI tool

```bash
  $ [sudo] npm install otfforge
```

## Usage

```js
const { src, dest } = require('gulp');
const otfforge = require('otfforge');

function convertFont(cb) {
    src("**/*.otf")
        .pipe(otfforge())
        .pipe(dest("fonts"));

    cb();
}
```

## License

[MIT](http://opensource.org/licenses/MIT)
