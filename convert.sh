#!/usr/local/bin/fontforge
Open($1);
fontFile = $fontname+".ttf";
Print('{');
Print('    "fontName": "' + $fontname + '",');
Print('    "familyName": "' + $familyname + '",');
Print('    "fullName": "' + $fullname + '",');
Print('    "fontVersion": "' + $fontversion + '",');
Print('    "fontFile": "' + fontFile + '"');
Print('}');

Generate($2+"/"+fontFile);
Quit(0);
