const exec = require('child_process').exec;
const fs = require('fs');
const os = require('os');
const through = require('through2');
const async = require('async');
const Vinyl = require('vinyl');

module.exports = (params) => {
    let fontInfo;
    let opts = params;

    if (opts === undefined) {
        opts = {
            debug: false,
        };
    }

    return through.obj(function (file, enc, cb) {
        async.series([
            function (callback) {
                exec(`fontforge -script "${__dirname}/convert.sh" "${file.path}" "${os.tmpdir()}"`, function (error, stdout, stderr) {
                    if (opts.debug === true) {
                        console.log(`stderr: ${stderr}`);
                    }

                    if (error === null) {
                        fontInfo = JSON.parse(stdout);
                        callback(null, fontInfo);
                    } else {
                        console.log(`exec error: ${error}`);
                    }
                });
            },
        ], function (err, results) {
            if (opts.debug === true) {
                console.log(`Font info: ${results[0]}`);
            }

            const fontFile = new Vinyl({
                cwd: '/',
                base: `${os.tmpdir()}/`,
                path: `${os.tmpdir()}/${fontInfo.fontFile}`,
                contents: fs.readFileSync(`${os.tmpdir()}/${fontInfo.fontFile}`),
            });

            fontFile.data = fontInfo;

            this.push(fontFile);

            return cb();
        }.bind(this));
    });
};
